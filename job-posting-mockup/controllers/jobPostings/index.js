const { checkAge, checkExprience, checkGender, modelGetJobPostingWithId, searchTextJobPosting } = require('./utils')
const searchJobPostingWithText = function (req, res) {
  res.status(200).json(searchTextJobPosting())
}

const getJobPostingWithId = function (req, res) {
  const id = parseInt(req.params.id)
  modelGetJobPostingWithId(id).then((result) => {
    res.status(200).json(result)
  }).catch((err) => {
    res.status(404).json({ message: err.message })
  })
}

const applyJobPosting = function (req, res, next) {
  const resume = req.body
  const id = parseInt(req.params.id)
  modelGetJobPostingWithId(id).then((jobPosting) => {
    if (!checkAge(resume.age, jobPosting.lowerBoundAge, jobPosting.upperBoundAge)) {
      res.status(422).json({
        message: 'Age out of range',
        resume,
        jobPosting
      })
      return
    }
    if (!checkExprience(resume.experience, jobPosting.experience)) {
      res.status(422).json({
        message: 'Not enough experience',
        resume,
        jobPosting
      })
      return
    }
    if (!checkGender(resume.gender, jobPosting.gender)) {
      res.status(422).json({
        message: 'Gender does not meet the condition',
        resume,
        jobPosting
      })
      return
    }

    res.status(200).json({
      message: 'Applied Jobs',
      resume,
      jobPosting
    })
  }).catch((err) => {
    res.status(404).json({ message: err.message })
  })
}

module.exports = {
  searchJobPostingWithText,
  getJobPostingWithId,
  applyJobPosting
}
